<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_4
 */

?>

	</div><!-- #content -->

</div><!-- #page (site) -->

<?php
get_template_part( 'footer_mdb' );
?>


<?php wp_footer(); ?>

</body>
</html>
