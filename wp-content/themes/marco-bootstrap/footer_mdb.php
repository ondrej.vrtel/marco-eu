<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-8 mt-md-0 mt-3">

        <!-- Content -->
        <p><strong>MARCO</strong> reklamní agentura, spol. s r.o.</p>
        <p><strong>① adresa</strong> Palackého třída 186, 612 00 Brno <strong>telefon</strong>&nbsp;+420 532 300 060 <strong>e-mail</strong>&nbsp;<a href="mailto:info@marco.eu">info@marco.eu</a></p>
        <p><strong>② adresa</strong> Bieblova 1227/19, 150 00 Praha 5 <strong>telefon</strong>&nbsp;+420 777 797 388 <strong>e-mail</strong>&nbsp;<a href="mailto:info@marco.eu">info@marco.eu</a></p>

      </div>
      <!-- Grid column -->

      <!-- <hr class="clearfix w-100 d-md-none pb-3"> -->

      <!-- Grid column -->
      <div class="col-md-4 mb-md-0 mb-3">

        <!-- Links -->
        <p>&nbsp;</p>

        <ul class="list-unstyled list-inline text-center">
          <li class="list-inline-item">
            <a href="#!">blog B2B</a><br>
            <img src="/wp-content/uploads/imgs/footer_img_social1.jpg" alt="" width="50px">
          </li>
          <li class="list-inline-item">
            <a href="#!">facebook</a><br>
            <img src="/wp-content/uploads/imgs/footer_img_social2.jpg" alt="" width="50px">
          </li>
          <li class="list-inline-item">
            <a href="#!">linkedin</a><br>
            <img src="/wp-content/uploads/imgs/footer_img_social3.jpg" alt="" width="50px">
          </li>
          <li class="list-inline-item">
            <a href="#!">youtube</a><br>
            <img src="/wp-content/uploads/imgs/footer_img_social4.jpg" alt="" width="50px">
          </li>

        </ul>

      </div>
      <!-- Grid column -->



    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 <strong>MARCO BBN</strong> Všechna práva vyhrazena. <a href="#">Ochrana osobních údajů</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
