<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">
    <!-- Grid row -->
    <div class="row">
      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">
        <!-- Content -->
        <h6 class="text-uppercase">MARCO reklamní agentura, spol. s r.o.</h6>
        <p>adresa Palackého třída 186, 612 00 Brno <br>
					telefon +420 532 300 060 <br>
					e-mail info@marco.eu<br>
					<!-- © 2019 Copyright: <a href="http://marco-eu.marco-creative.cz"> Marco BBN</a> -->
				</p>
      </div>
      <!-- Grid column -->
      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">
				<!-- blog -->
          <a href="#"> blog B2B </a>
				<!-- Facebook -->
          <a href="#"> facebook </a>
          <!--Linkedin -->
          <a href="#"> linkedin </a>
          <!--youtube -->
          <a href="#"> youtube </a>
      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row -->
  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-1">© 2019 Copyright:
    <a href="http://marco-eu.marco-creative.cz"> Marco BBN</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
