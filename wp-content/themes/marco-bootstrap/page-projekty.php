<?php
/*
* Template Name: projekt mustr
*/

get_header(); ?>

<div class="submenu">
    <ul class="nav sec-menu">
      <a class="nav-link" href=""><li class="nav-item">Vše</li></a>
      <a class="nav-link" href=""><li class="nav-item">Corporate Identity</li></a>
      <a class="nav-link" href=""><li class="nav-item">Tiskoviny</li></a>
      <a class="nav-link" href=""><li class="nav-item">Web</li></a>
      <a class="nav-link" href=""><li class="nav-item">Reklamní kampaň</li></a>
    </ul>
</div>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">

              <div class="grid">
                <div class="grid-sizer"></div>
<?php

// if ( have_posts() ) {
// 	while ( have_posts() ) {
// 		the_post();
// 		//
// 		echo "<pre>"; print_r($posts); echo "</pre>";
// 		//
// 	} // end while
// } // end if

$posts = get_posts(array(
  'post_type'        => 'projekty',
  'posts_per_page'   => -1,
  // 'orderby'          => 'date',
  // 'order'            => 'DESC',
  'suppress_filters' => true
));


    foreach ($posts as $post ) : ?>

                  <div class="grid-item">
                    <a href="<?php echo esc_url(get_permalink($post->ID)); ?>">
                    <?php the_post_thumbnail(); ?>
                      <!-- <img src="<?php echo get_stylesheet_directory_uri().'/images/image-1.jpg' ?>"> -->
                    </a>
                    <?php $categories = get_the_terms( $post->ID, 'zakaznik' ); ?>
                    <p>● <?php echo $categories[0]->name ?> ● <?php echo apply_filters( 'the_title', $post->post_title ) ?><br />
                       <?php if ( get_field('perex_do_vypisu') ) echo '○ '. get_field('perex_do_vypisu'); ?>
                    </p>
                  </div>

    <?php endforeach ?>
              </div>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
