<?php global $nth_slide; ?>
<div class="row-flex">
  <div class="section-number">
  </div>
  <div class="section-content">
    <h1><?php echo get_sub_field('title_h1') ?></h1>
    <p><?php  echo do_shortcode(get_sub_field('content'))  ?></p>
  </div>
</div>
