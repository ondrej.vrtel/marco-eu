<?php global $nth_slide; ?>
<div class="row-flex">
  <div class="section-number">
    <div class="number circle"><?php echo $nth_slide-1 ?></div>
  </div>
  <div class="section-content">
    <h1><?php echo get_sub_field('title_h1') ?></h1>
    <p><?php  echo do_shortcode(get_sub_field('content'))  ?></p>
    <?php
      $linkList = get_sub_field('link-list');
      if ( $linkList ) {
        $linkArray = explode("\n", $linkList);
        echo '<ul class="list-group list-group-horizontal inline">';

        foreach ($linkArray as $linkItem) {
          $link = explode("#", $linkItem);

          // create the html
        	$linkhtml  = '<li class="list-group-item border-0 p-0">';
        	$linkhtml .= '<a href="'. $link[1] .'" class="black inline sipka">';
        	$linkhtml .= 	$link[0];
        	$linkhtml .= '</a></li>';
          echo $linkhtml;

          // echo "<pre>"; print_r( $link ); echo "</pre>";
        }
        echo "</ul>";
      }

     ?>

  </div>
</div>
