<?php

get_header(); ?>

<div id="secmenu" class="submenu" style="z-index: 99;">
    <ol class="nav sec-menu circle">
      <?php
        $n = 1;
        if( have_rows('sekce') ): while ( have_rows('sekce') ) : the_row();

          echo '<li data-menuanchor="page'. $n .'" class="nav-item'. ( $n == 1 ? ' active' : '' ) .'"><a class="nav-link" href="#page'. $n .'">';
          if ( 1 != $n )  {
            echo '<span>';
            echo $n-1;
            echo '</span> ' . get_sub_field('menu_title');
          }
          echo '</a></li>';
          // $n++;
          $n++;
        endwhile; endif;
      ?>
    </ol>
</div>

  <div id="fullpage">


    <?php
     $nth_slide = 0;
      if( have_rows('sekce') ):
        while ( have_rows('sekce') ) : the_row();
        // když je bg obrázek, nastaví písmo na white
        // echo '<div class="section" id="section'. $nth_slide . ( get_sub_field('bg_img') == '' ? '" style="color: black;">' : '" style="color: white; background-image: url(' . get_sub_field('bg_img') . ')">' );
        // upraveno pro sekci Co děláme, bílou jen u prvého obrázku
        echo '<div class="section" id="section'. $nth_slide . ( $nth_slide == 0 ? '" style="color: white; ' : '" style="color: black;') . ' background-image: url(' . get_sub_field('bg_img') . ')">';
        $nth_slide++;

        get_template_part( 'co-delame/blok', get_sub_field('template_file') );
    ?>

    </div>
      <?php endwhile; endif; ?>

  	<!-- Vzor sekcí
      <div class="section" id="section0">
        <h1>Specializujeme se <br>na B2B marketing!</h1>
        <p>Jsme integrovaná komunikační agentura poskytující ... </p>
      </div>
    -->

<!-- end full page -->
  </div>



  <!-- <div class="section" id="section1">
    <div class="slide" id="slide1"><h1>Slide Backgrounds</h1></div>
    <div class="slide" id="slide2"><h1>Totally customizable</h1></div>
  </div> -->

<?php
get_footer();
