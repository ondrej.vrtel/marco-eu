<?php

/**
 * ZÁKLADNÍ NASTAVENÍ JE V MARCO-TEMPLATE-PLUGINS
 */

	 add_action( 'wp_enqueue_scripts', 'marco_bootstrap_enqueue_styles' );
	 function marco_bootstrap_enqueue_styles() {
 		  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); /* stylu z rodičovské šablony*/
		}
			// styly z child šablony

			// stránka o nás
			add_action( 'wp_enqueue_scripts', 'slidesNav_enqueue_script' );
			function slidesNav_enqueue_script() {
				if (is_page( array( 'o-nas', 'co-delame' ))) {
					wp_enqueue_style( 'slidesNav', THEME_DIR . '/slidesNav/css/fullpage.css' );
					wp_enqueue_script( 'slidesNavscript', THEME_DIR . '/slidesNav/js/fullpage.js', __FILE__ , array('jquery'), '1.0', true );
					wp_enqueue_script( 'slidesInit', THEME_DIR . '/slidesNav/js/init.js', __FILE__ , array('slidesNavscript'), '1.0', true );
				}
				if (is_page( 'o-nas' )) {
					wp_enqueue_script( 'kontaktabs', THEME_DIR . '/js/kontaktabs.js', __FILE__ , array('jquery'), '1.0', true );
				}
			}

/**
 * define NUMBER IN CIRCLE UNICODE ARRAY
 */

			define('DIGITS', array( '0','➀','➁','➂','➃','➄','➅','➆','➇','➈','➉'));
			define('NDIGITS', array( '0','➊','➋','➌','➍','➎','➏','➐','➑','➒','➓'));


 ?>
