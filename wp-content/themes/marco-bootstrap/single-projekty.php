<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<?php
// echo "<pre>"; print_r( $wp_query->posts ); echo "</pre>";

if( have_posts() ) :
	while ( have_posts() ) : the_post()
	?>
<?php $tax_zakaznik = get_the_terms( $post->ID, 'zakaznik' ); ?>
<?php $tax_obor = get_the_terms( $post->ID, 'obor' ); ?>
<?php $tax_typ = get_the_terms( $post->ID, 'typ_projektu' ); ?>

<div class="submenu">
    <ul class="nav sec-menu">
      <a class="nav-link" href=""><li class="nav-item"><?php echo $tax_zakaznik[0]->name ?></li></a>
      <a class="nav-link" href=""><li class="nav-item"><?php echo $tax_obor[0]->name ?></li></a>
      <a class="nav-link" href=""><li class="nav-item"><?php echo $tax_typ[0]->name ?></li></a>
    </ul>
</div>

<div class="uvodni-obrazek">
	<?php
	if ( get_field('uvodni_obrazek') ) {
		echo '<div class="post-thumbnail"><img src="' . get_field('uvodni_obrazek') . '" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="">	</div>';
	} else {
		wp_bootstrap_4_post_thumbnail();
	}
	 ?>

</div>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">

              <div class="section_flex">

								<!-- Title menu form repeater -->
                <div class="proj_menu sticky">
										<?php
										if ( get_field('custom_list') ):
											$custom_array = explode( "\xA", get_field('custom_list'));
											// print_r($custom_array);
											echo '<ul>';
											foreach ($custom_array as $item) {
												echo '<li class="nav-item">' . $item . '</li>';
											}
											echo '</ul>';
										elseif( have_rows('slide') ):
											echo '<ul>';
											while ( have_rows('slide') ) : the_row();
											echo '<li class="nav-item">' . get_sub_field('title') . '</li>';
											endwhile;?>
											</ul>
										<?php
										else : echo '« Nejsou žádné slajdy. »';
										endif;
										?>
                </div>

	                <div class="proj_popis">
	                  <div class="title">
	                    <h1><?php the_title( $before = '', $after = '', $echo = true ) ?></h1>
	                  </div>
										<div class="content">
											<?php the_content(); ?>
										</div>
	                  <!-- <?php $my_excerpt = get_the_excerpt(); if ( $my_excerpt ) { echo '<p class="perex">' . $my_excerpt . '</p>'; }; ?> -->

										<!--
										**  colapse menu
										<p>
											<a class="arrow_vice collapsed" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">&nbsp;</a>
										</p>
										<div class="collapse" id="collapseExample">
										</div> -->


                </div>
                <!-- end div uvodni popis section flex -->
              </div>


							<?php
							$nth_slide = 0;
							if( have_rows('slide') ):
								while ( have_rows('slide') ) : the_row();

								$nth_slide++;

								echo '<div class="slide_flex">';
								echo '<div class="proj_part sticky"><div class="body">' . get_sub_field('caption') . '</div></div>';

								$medium = get_sub_field('medium');
								// echo $medium;

								// ###### OBRÁZEK
								if ( 'img' == $medium ) {
									$obrazek = get_sub_field('obrazek');
									if ( $obrazek ) {
										echo '<div class="proj_img"><img src="' . $obrazek . '" alt=""></div>';
									} else echo '<div class="alert alert-danger" role="alert"> Není vybrán obrázek!</div>';
									// echo get_sub_field('obrazek');
								}

								// ###### VIDEO
								if ( 'vid' == $medium ) {
									$video = get_sub_field('video');
									if ( $video ) {
										echo '<div class="proj_img"><div class="vid_container">' . get_sub_field('video') . '</div></div>';
									} else echo '<div class="alert alert-danger" role="alert"> Není zadáno video!</div>';
								}
								// ###### SLAJDY
								if ( 'slide' == $medium ) {
									$slides = get_sub_field('slides');
									if ( $slides ) {

										echo '<div class="proj_img">
													<div id="slide'.$nth_slide.'" class="carousel slide carousel-fade" data-ride="carousel">
														<ul class="carousel-indicators">
												';

												// echo "<pre>"; print_r( $slides ); echo "</pre>";
										foreach ($slides as $n => $slide) {
											echo '<li data-target="#slide'.$nth_slide.'" data-slide-to="' . $n . '"';
											if ( !$n ) echo ' class="active"';
											echo '></li>';
										}
										echo '</ul>';
										echo '<div class="carousel-inner">';
										foreach ($slides as $n => $slide) {
												echo '<div class="carousel-item';
												if ( !$n ) echo ' active';
												echo '"><img src="'.$slide['url'].'" alt=""></div>';
											}
										echo '</div">';

										// ##### ŠIPKY
										echo '<!-- Left and right controls -->
				                  <a class="carousel-control-prev" href="#slide'.$nth_slide.'" data-slide="prev">
				                    <span class="carousel-control-prev-icon"></span></a>
				                  <a class="carousel-control-next" href="#slide'.$nth_slide.'" data-slide="next">
				                    <span class="carousel-control-next-icon"></span></a>
													';
									// End     carousel-inner | slide_number | proj_img
									echo '</div></div></div>';
								} else echo '<div class="alert alert-danger" role="alert"> Nejsou vybrány obrázky do slideru!</div>';
								}

								// End slide_flex
								echo '</div>';
								endwhile;?>
							</div>
							<?php
							else : echo '« Nejsou žádné slajdy ».';
							endif;
							?>


<?php
					endwhile;
					endif;
 ?>

<!--  prev/ next -->

            <div class="section_flex navigace">

              <div class="p_prev">
                <h3><?php previous_post_link( $format = '%link', $link = '%title', $in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ); ?></h3>
                <div class="prevnext">&laquo; předchozí projekt</div>
              </div>
              <div class="p_next">
                <h3><?php next_post_link( $format = '%link', $link = '%title', $in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ); ?></h3>
                <p>následující projekt &raquo;</p>
              </div>
            </div>
              <div class="d-flex justify-content-center">
                  <h3><a href="/projekty/">zpět na projekty</a></h3>
              </div>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php


get_footer();
