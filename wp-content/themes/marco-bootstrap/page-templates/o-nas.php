<?php
/*
* Template Name: O-nas
*/


get_header(); ?>

<div id="secmenu" class="submenu" style="z-index: 99;">
    <ol class="nav sec-menu circle">
      <li data-menuanchor="page1" class="nav-item active"><a class="nav-link" href="#page1"><span>1</span> kdo jsme</a></li>
      <li data-menuanchor="page2" class="nav-item"><a class="nav-link" href="#page2"><span>2</span> proč s námi?</a></li>
      <li data-menuanchor="page3" class="nav-item"><a class="nav-link" href="#page3"><span>3</span> klienti o nás</a></li>
      <li data-menuanchor="page4" class="nav-item"><a class="nav-link" href="#page4"><span>4</span> BBN</a></li>
      <li data-menuanchor="page5" class="nav-item"><a class="nav-link" href="#page5"><span>5</span> partneři</a></li>
      <li data-menuanchor="page6" class="nav-item"><a class="nav-link" href="#page6"><span>6</span> kontakty</a></li>
      <li class="nav-item"><a class="nav-link" href="/novinky/"><span>7</span> novinky</a></li>
    </ol>
</div>

  <div id="fullpage">
  	<div class="section" id="section0">
      <h1>Specializujeme se <br>na B2B marketing</h1>
      <p>Jsme integrovaná komunikační agentura poskytující kreativní a strategický marketingový servis. Stavíme na zkušenostech, moderním myšlení a spolehlivém accountském servisu. To vše v kombinaci s nejmodernějšími nástroji a ověřeným, unikátním know how.</p>
    </div>
  	<div class="section" id="section1">
      <h1>Proč pracovat <br>s MARCO BBN?</h1>
      <p>Prošli jsme si vývojem bez kotrmelců, postupně od našeho založení v roce 1994 rosteme. Naše úspěchy jsou spojeny se stabilním agenturním týmem, který je kombinací zkušenosti, talentu a dravého mládí. Jsme součástí mezinárodní agentury BBN – The world`s b2b agency, která nám i našim klientům umožňuje překročit hranice. Našim lokálním zadavatelům díky BBN přinášíme
ověřené mezinárodní know how.</p>
    </div>
  	<div class="section" id="section2"><h1>Klienti o nás</h1></div>
  	<div class="section" id="section3">
      <h1>BBN – The worlds's b2b agency</h1>
      <p>BBN je celosvětová síť B2B marketingových agentur, do které je zapojeno více než 1100 specialistů z 29 marketingových agentur v 21 zemích. Celosvětová působnost BBN je postavena na nezávislých a silných lokálních marketingových agenturách se zkušeným vedením.</p>
    </div>
  	<div class="section" id="section4"><h1>partneři</h1></div>
  	<div class="section" id="section5"><h1>kontakty</h1></div>
  </div>



  <!-- <div class="section" id="section1">
    <div class="slide" id="slide1"><h1>Slide Backgrounds</h1></div>
    <div class="slide" id="slide2"><h1>Totally customizable</h1></div>
  </div> -->

<?php
get_footer();
