<?php
/*
* Template Name: projekty
*/

get_header(); ?>

<div class="submenu">
    <ul class="nav sec-menu">
      <a class="nav-link" href=""><li class="nav-item">GCE</li></a>
      <a class="nav-link" href=""><li class="nav-item">rebranding identity</li></a>
      <a class="nav-link" href=""><li class="nav-item">company</li></a>
    </ul>
</div>

<?php wp_bootstrap_4_post_thumbnail(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">

              <div class="section_flex">

                <div class="proj_menu sticky">
                  <ul>
                    <li class="nav-item">Brand Portfolio Organiser</li>
                    <li class="nav-item">Brand Book</li>
                    <li class="nav-item">Corporate Identity manuál značek</li>
                    <li class="nav-item">Guidelines pro tvorbu vizuálních výstupů</li>
                    <li class="nav-item">Office branding manuál</li>
                    <li class="nav-item">Video prezentující rebrandový proces</li>
                    <li class="nav-item">Školící materiály pro interní workshopy</li>
                  </ul>
                </div>
                <?php
                while ( have_posts() ) : the_post();
                ?>
                <div class="proj_popis">
                  <div class="title">
                    <h1>GCE<br>Celosvětový<br>rebranding</h1>
                  </div>
                  <?php
                      $my_excerpt = get_the_excerpt();
                      if ( '' != $my_excerpt ) { };
                      // echo $my_excerpt;
                  ?>
                  <p>
                    Rychlý růst společnosti, expanze na nových trzích a akvizice u nových společností nekontrolovaně měnila tvář společnosti. Byli jsme osloveni,
                    abychom vytvořili novou vizuální identitu společnosti a zakotvili ji do uceleného manuálu. Po prvních setkáních se zákazníkem jsme společně došli k závěru,
                    že je třeba se na zadání podívat ze širšího hlediska. Nakonec se zákazník rozhodl projít kompletním rebrandovým procesem, pro který jsme použili osvědčenou metodologii BBN
                    s nástroji definovanými v Brand Asset Managementu. bla bla bla bla bla podruhé
                  </p>
                  <p>
                    <a class="arrow_vice" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">&nbsp;</a>
                  </p>
                  <div class="collapse" id="collapseExample">

                    <?php


                    get_template_part( 'template-parts/content', 'page-full' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                      comments_template();
                    endif;

                  endwhile; // End of the loop.
                  ?>

                </div>
                </div>
                <div class=""> </div>
              </div>

            <div class="section_flex">

              <div class="proj_part sticky">
                01 Brand Portfolio organiser <br>
                design Michal Pulmann <br>
                fotografie Ondřej Vrtěl <br>
                © All rights reserved <br>
              </div>
              <div class="proj_img">
                <div id="demo" class="carousel slide" data-ride="carousel">

                  <!-- Indicators -->
                  <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                  </ul>

                  <!-- The slideshow -->
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="/wp-content/uploads/2019/06/gce_21.jpg" alt="Los Angeles">
                    </div>
                    <div class="carousel-item">
                      <img src="/wp-content/uploads/2019/06/gce_22.jpg" alt="Chicago">
                    </div>
                    <div class="carousel-item">
                      <img src="/wp-content/uploads/2019/06/gce_23.jpg" alt="New York">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>

                </div>
                <!-- <img src="/wp-content/uploads/2019/06/gce_3.jpg" alt=""> -->

              </div>
            </div>

            <div class="section_flex">

              <div class="proj_img">
                <img src="/wp-content/uploads/2019/06/gce_3.jpg" alt="">
              </div>
              <div class="proj_part sticky">
                02 Brand Portfolio organiser <br>
                design Michal Pulmann <br>
                fotografie Ondřej Vrtěl <br>
                © All rights reserved <br>
              </div>
            </div>
            <div class="section_flex">

              <div class="proj_part sticky">
                03 Brand Portfolio organiser <br>
                design Michal Pulmann <br>
                fotografie Ondřej Vrtěl <br>
                © All rights reserved <br>
              </div>
              <div class="proj_img">
                <img src="/wp-content/uploads/2019/06/gce_4.jpg" alt="">
              </div>
            </div>
            <div class="section_flex">

              <div class="proj_img">
                <img src="/wp-content/uploads/2019/06/gce_5.jpg" alt="">
              </div>
              <div class="proj_part">
                04 Brand Portfolio organiser <br>
                design Michal Pulmann <br>
                fotografie Ondřej Vrtěl <br>
                © All rights reserved <br>
              </div>
            </div>

<!--  prev/ next -->

            <div class="section_flex">

              <div class="p_prev">
                <h3>I am Brno </h3>
                <p>předchozí studie</p>
              </div>
              <div class="p_next">
                <h3>The Aluternative</h3>
                <p>následující studie</p>
              </div>
            </div>
              <div class="d-flex justify-content-center">
                  <h3><a href="#">zpět na projekty</a></h3>
              </div>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
