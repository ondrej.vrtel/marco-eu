<?php
/*
* Template Name: Full Width do Shortcode
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-fluid">
            <main id="main" class="site-main">

                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'page-full-shortcode' );



                endwhile; // End of the loop.
                ?>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
