<?php global $nth_slide; ?>
<div class="row-flex">
  <div class="section-number">
    <div class="number circle"><?php echo $nth_slide ?></div>
  </div>
  <div class="section-content">
    <h1><?php echo get_sub_field('title_h1') ?></h1>
    <!-- <p>repeater klienti</p> -->




    <?php
    echo '<div class="klienti">
            <div id="slide'.$nth_slide.'" class="carousel slide" data-ride="carousel">
              <ul class="carousel-indicators">
          ';
    $slides = get_field('feedbacks');

    // echo "<pre>"; print_r( $slides ); echo "</pre>";
    foreach ($slides as $n => $slide) {
      echo '<li data-target="#slide'.$nth_slide.'" data-slide-to="' . $n . '"';
      if ( !$n ) echo ' class="active"';
      echo '></li>';
    }
    echo '</ul>';
    echo '<div class="carousel-inner">';
    foreach ($slides as $n => $slide) {
      echo '<div class="carousel-item';
      if ( !$n ) echo ' active';
      echo '">';
      ?>

        <h2><?php echo $slide['firma']; ?></h2>

        <br>
        <h4><?php echo $slide['jmeno']; ?></h4>
        <br>
        <p><?php echo $slide['content']; ?></p>

      <?php
      echo '</div>';
    }
    echo '</div">';

    // ##### ŠIPKY
    echo '<!-- Left and right controls -->
          <a class="carousel-control-prev" href="#slide'.$nth_slide.'" data-slide="prev">
            <span class="carousel-control-prev-icon"></span></a>
          <a class="carousel-control-next" href="#slide'.$nth_slide.'" data-slide="next">
            <span class="carousel-control-next-icon"></span></a>
          ';
    // End     carousel-inner | slide_number | klienti
    echo '</div></div></div>';
    ?>
  </div>
</div>
