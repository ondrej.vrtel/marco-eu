<?php global $nth_slide; ?>
<div class="row-flex">
  <div class="section-number">
    <div class="number circle"><?php echo $nth_slide ?></div>
  </div>
  <div class="section-content">
    <h1><?php echo get_sub_field('title_h1') ?></h1>

    <div class="kontakt">
      <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'Brno')" id="defaultOpen">Brno</button>
        <button class="tablinks" onclick="openCity(event, 'Praha')">Praha</button>
      </div>

      <div id="Brno" class="tabcontent">
        <p>MARCO<br>
          reklamní agentura, spol. s r.o<br>
          Palackého třída 18<br>
          612 00 Brno </p>
          <p> tel. +420 532 300 060<br>
          e-mail info@marco.eu</p>
          <p> <?php echo do_shortcode( '[button link="/kdo-je-marco/" color="white"]Kdo je Marco?[/button]' ) ?> </p>
      </div>

      <div id="Praha" class="tabcontent">
        <p>MARCO<br>
          reklamní agentura, spol. s r.o<br>
          Bieblova 1227/19<br>
          150 00 Praha 5 </p>
          <p> mobil +420 777 797 388, tel. +420 251 560 640<br>
          e-mail praha@marco.eu</p>
          <p> <?php echo do_shortcode( '[button link="/kdo-je-marco/" color="white"]Kdo je Marco?[/button]' ) ?> </p>
      </div>

      <div class="icodic">
        <p> IČ 60702265<br>
        DIČ CZ60702265<br>
        Společnost zapsána u KOS<br>
        v Brně, oddíl C, vložka 14776</p>
      </div>
      <div class="maplink">
        <?php echo do_shortcode( '[button link="/mapa/" color="white"]Ukázat na mapě[/button]' ) ?>
      </div>

    </div>


  </div>
</div>
