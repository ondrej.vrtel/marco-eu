<?php global $nth_slide; ?>
<div class="row-flex">
  <div class="section-number">
    <div class="number circle"><?php echo $nth_slide ?></div>
  </div>
  <div class="section-content">
    <h1><?php echo get_sub_field('title_h1') ?></h1>
    <!-- <p>repeater partneri 2</p> -->
  </div>
</div>
    <div class="loga-partneru">
        <?php
        if( have_rows('partneri') ):
          echo '<ul class="loga">';
          while ( have_rows('partneri') ) : the_row();
          echo '<li class="logo">' . get_sub_field('firma') . '</li>';
          endwhile;?>
          </ul>
        <?php
        else : echo '« Nejsou žádná loga. »';
        endif;
        ?>
    </div>
