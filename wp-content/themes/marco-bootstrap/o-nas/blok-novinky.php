<?php global $nth_slide; ?>
<div class="row-flex">
  <div class="section-number">
    <div class="number circle"><?php echo $nth_slide ?></div>
  </div>
  <div class="section-content">
    <h1><?php echo get_sub_field('title_h1') ?></h1>
    <!-- <p>repeater partneri 2</p> -->
  </div>
</div>
    <div class="row-flex">
        <?php

        $posts = get_posts(array(
          'post_type'        => 'novinky',
          'posts_per_page'   => 3,
          'orderby'          => 'date',
          'order'            => 'DESC',
          'suppress_filters' => true
        ));


            foreach ($posts as $post ) : ?>

                          <div class="grid-novinka">
                            <?php if ( get_field('odkaz') ) {
                              $ext_url = get_field('odkaz');
                              echo '<a href="' . $ext_url . '" target="_blank">';
                              the_post_thumbnail();
                              if ( get_field('zdroj')) echo '<div class="zdroj">' . get_field('zdroj') . '</div>';
                              echo '</a>';
                            } else the_post_thumbnail();
                            ?>
                            <h2>
                              <?php
                                if ( $ext_url ) {
                                  echo '<a href="' . $ext_url . '" target="_blank">';
                                  echo apply_filters( 'the_title', $post->post_title );
                                  echo '</a>';
                                } else echo apply_filters( 'the_title', $post->post_title )
                              ?>
                            </h2>
                          </div>

            <?php endforeach ?>



    </div>
