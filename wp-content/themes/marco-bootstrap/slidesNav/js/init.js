var myFullpage = new fullpage('#fullpage', {
    verticalCentered: false,
    anchors: ['page1', 'page2', 'page3', 'page4', 'page5', 'page6', 'page7'],
    // navigation: true,
    // navigationPosition: 'left',
    // navigationTooltips: [ '1 kdo jsme', '2 proč s námi?', '3 klienti o nás', '4 BBN', '5 partneři', '6 kontakty'],
    menu: '#secmenu',
    lazyLoad: true

    //to avoid problems with css3 transforms and fixed elements in Chrome, as detailed here: https://github.com/alvarotrigo/fullPage.js/issues/208       css3:false
});
