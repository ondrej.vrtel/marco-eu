<?php

get_header(); ?>

<div id="secmenu" class="submenu" style="z-index: 99;">
    <ol class="nav sec-menu circle">
      <?php
        $n = 1;
        if( have_rows('sekce') ): while ( have_rows('sekce') ) : the_row();

          echo '<li data-menuanchor="page'. $n .'" class="nav-item'. ( $n == 1 ? ' active' : '' ) .'"><a class="nav-link" href="#page'. $n .'"><span>'. $n .'</span> ';
          echo get_sub_field('menu_title');
          echo '</a></li>';
          $n++;
        endwhile; endif;
      ?>
    </ol>
</div>

  <div id="fullpage">


    <?php
     $nth_slide = 0;
      if( have_rows('sekce') ):
        while ( have_rows('sekce') ) : the_row();
        echo '<div class="section" id="section'. $nth_slide . ( get_sub_field('bg_img') == '' ? '" style="color: black;">' : '" style="color: white; background-image: url(' . get_sub_field('bg_img') . ')">' );
        $nth_slide++;

        get_template_part( 'o-nas/blok', get_sub_field('template_file') );
    ?>

    </div>
      <?php endwhile; endif; ?>


<!-- end full page -->
  </div>

<?php
get_footer();
